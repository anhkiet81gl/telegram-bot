import TelegramBot from 'node-telegram-bot-api';
import fetch from 'node-fetch';
import 'dotenv/config';

const typeEnum = ['food', 'transport', 'shopping', 'housing', 'entertainment', 'borrowing', 'other'];
const bot = new TelegramBot(process.env.BOT_TOKEN, {
    polling: true
});

bot.onText(/\/start/, (msg) => {
    const chatId = msg.chat.id;
    bot.sendMessage(chatId, '📝 Hướng dẫn sử dụng:\n\n' +
        '1. /list: Xem danh sách chi tiêu\n' +
        '2. /add type|name|price: Thêm chi tiêu\n\n' +
        'Ví dụ:\n```\n/add food|name|price|note\n```', {
            parse_mode: 'Markdown'
        });
});

bot.onText(/\/bot (.+)/, (msg, match) => {
    const chatId = msg.chat.id;
    const resp = match[1];

    bot.sendChatAction(chatId, 'typing');

    switch (resp) {
        case '-h':
            bot.sendMessage(chatId, '📝 Hướng dẫn sử dụng:\n\n' +
                '1. /list: Xem danh sách chi tiêu\n' +
                '2. /add type|name|price|note: Thêm chi tiêu\n\n' +
                'Ví dụ:\n```\n/add food|name|price\n```', {
                    parse_mode: 'Markdown'
                });
            break;
        case '-type':
            bot.sendMessage(chatId, '📝 Loại chi tiêu:\n\n' +
                '1. food: ăn uống\n' +
                '2. transport: đi lại\n' +
                '3. shopping: mua sắm\n' +
                '4. housing: nhà cửa\n' +
                '5. entertainment: giải trí\n' +
                '6. borrowing: vay mượn\n' +
                '7. other: khác', {
                    parse_mode: 'Markdown'
                });
            break;
        default:
            bot.sendMessage(chatId, '📝 Hướng dẫn sử dụng: Ví dụ /bot -h)', {
                parse_mode: 'Markdown'
            });
    }
});

bot.onText(/\/add (.+)/, (msg, match) => {
    const chatId = msg.chat.id;

    bot.sendChatAction(chatId, 'typing');

    //check input
    if (!match[1].includes('|')) {
        bot.sendMessage(chatId, 'Vui lòng nhập đúng định dạng.' + '\n\n' + 'Ví dụ:\n```\n/add type|name|price|note\n```', {
            parse_mode: 'Markdown'
        });
        return;
    }

    const resp = match[1];
    const values = resp.split('|');

    //check type
    if (!typeEnum.includes(values[0])) {
        bot.sendMessage(chatId, 'Vui lòng nhập đúng loại chi tiêu.' + '\n\n' + 'Ví dụ:\n```\nfood|name|price|note\n```', {
            parse_mode: 'Markdown'
        });
        return;
    }

    const url = new URL(process.env.WEBHOOK_URL);

    var timestamp = Date.now();

    // Tạo một đối tượng Date từ timestamp
    var date = new Date(timestamp);

    // Lấy thông tin ngày, tháng, năm, giờ, phút, giây
    var year = date.getFullYear();
    var month = ('0' + (date.getMonth() + 1)).slice(-2); // Tháng bắt đầu từ 0 nên cần cộng thêm 1
    var day = ('0' + date.getDate()).slice(-2);
    var hours = ('0' + date.getHours()).slice(-2);
    var minutes = ('0' + date.getMinutes()).slice(-2);
    var seconds = ('0' + date.getSeconds()).slice(-2);

    // Định dạng thành chuỗi yyyy-mm-dd hh:mm:ss
    var formattedDateTime = year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds;

    url.searchParams.append('date', formattedDateTime);
    url.searchParams.append('type', values[0]);
    url.searchParams.append('name', values[1]);
    url.searchParams.append('price', values[2]);
    url.searchParams.append('note', typeof values[3] === "undefined" ? '' : values[3]);

    fetch(url)
        .then(res => res.json())
        .then(data => {
            if (data.status === 'success') {
                bot.sendMessage(chatId, '✅ Đã thêm thành công.');
            } else {
                bot.sendMessage(chatId, 'Không thể thêm. Vui lòng thử lại sau!');
            }
        })
        .catch(err => {
            bot.sendMessage(chatId, 'Đã có lỗi xảy ra. Vui lòng thử lại sau!');
        });
});

console.log('Bot is running...')